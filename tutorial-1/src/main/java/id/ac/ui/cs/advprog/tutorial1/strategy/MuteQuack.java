package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MuteQuack implements QuackBehavior{
    @Override
    public void quack() {
        System.out.println("*complete utter silence as i am a duck that does not quack*");
    }
}
